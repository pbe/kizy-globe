

var data = [
    {
        "status": "ok",
        "lasttime": "2014-07-17 15:21:46",
        "firsttime": "2014-07-17 09:50:46",
        "interval": 19860,
        "lon": 7.07791310930109,
        "duration": "00:05:31:00",
        "radius": 1061.9304421836825,
        "lat": 46.76868843337613,
        "recvfirst": "2014-07-17 09:52:11"
    },
    {
        "status": "ok",
        "lasttime": "2014-07-17 15:36:02",
        "firsttime": "2014-07-17 15:36:02",
        "interval": 0,
        "lon": 7.184636,
        "duration": "00:00:00:00",
        "radius": 1907,
        "lat": 46.831157499999996,
        "recvfirst": "2014-07-17 15:43:19"
    },
    {
        "status": "ok",
        "lasttime": "2014-07-17 15:51:19",
        "firsttime": "2014-07-17 15:51:19",
        "interval": 0,
        "lon": 7.1228561,
        "duration": "00:00:00:00",
        "radius": 1316,
        "lat": 46.9274221,
        "recvfirst": "2014-07-17 15:58:34"
    },
    {
        "status": "ok",
        "lasttime": "2014-07-17 16:06:34",
        "firsttime": "2014-07-17 16:06:34",
        "interval": 0,
        "lon": 7.054678399999999,
        "duration": "00:00:00:00",
        "radius": 2847,
        "lat": 47.005936,
        "recvfirst": "2014-07-17 16:13:50"
    },
    {
        "status": "ok",
        "lasttime": "2014-07-17 16:36:12",
        "firsttime": "2014-07-17 16:20:55",
        "interval": 917,
        "lon": 6.984798263780728,
        "duration": "00:00:15:17",
        "radius": 915.4696477754248,
        "lat": 47.00984767479483,
        "recvfirst": "2014-07-17 16:29:12"
    },
    {
        "status": "ok",
        "lasttime": "2014-07-17 17:06:43",
        "firsttime": "2014-07-17 16:51:27",
        "interval": 916,
        "lon": 6.9328011891267565,
        "duration": "00:00:15:16",
        "radius": 1370.693171622712,
        "lat": 47.019005572887025,
        "recvfirst": "2014-07-17 16:59:45"
    },
    {
        "status": "ok",
        "lasttime": "2014-07-18 03:04:45",
        "firsttime": "2014-07-17 17:20:59",
        "interval": 35026,
        "lon": 7.002350096125566,
        "duration": "00:09:43:46",
        "radius": 1557.8206142146578,
        "lat": 47.13828094726456,
        "recvfirst": "2014-07-17 17:30:17"
    },
    {
        "status": "ok",
        "lasttime": "2014-07-18 03:19:01",
        "firsttime": "2014-07-18 03:19:01",
        "interval": 0,
        "lon": 6.9437921,
        "duration": "00:00:00:00",
        "radius": 2516,
        "lat": 47.0634167,
        "recvfirst": "2014-07-18 03:27:18"
    },
    {
        "status": "ok",
        "lasttime": "2014-07-18 03:34:18",
        "firsttime": "2014-07-18 03:34:18",
        "interval": 0,
        "lon": 7.052842399999999,
        "duration": "00:00:00:00",
        "radius": 1726,
        "lat": 47.0102112,
        "recvfirst": "2014-07-18 03:42:35"
    },
    {
        "status": "ok",
        "lasttime": "2014-07-18 03:49:35",
        "firsttime": "2014-07-18 03:49:35",
        "interval": 0,
        "lon": 7.113911,
        "duration": "00:00:00:00",
        "radius": 1762,
        "lat": 46.885596799999995,
        "recvfirst": "2014-07-18 03:57:53"
    },
    {
        "status": "ok",
        "lasttime": "2014-07-18 08:21:58",
        "firsttime": "2014-07-18 04:03:52",
        "interval": 15486,
        "lon": 7.081798523333018,
        "duration": "00:04:18:06",
        "radius": 1117.8214401650116,
        "lat": 46.7668327745726,
        "recvfirst": "2014-07-18 04:13:09"
    },
    {
        "status": "absent",
        "lasttime": "2014-07-18 08:49:28",
        "firsttime": "2014-07-18 08:49:28",
        "interval": 0,
        "duration": "00:00:00:00",
        "recvfirst": "2014-07-18 08:51:41"
    },
    {
        "status": "ok",
        "lasttime": "2014-07-18 09:17:03",
        "firsttime": "2014-07-18 09:04:42",
        "interval": 741,
        "lon": 7.0962173,
        "duration": "00:00:12:21",
        "radius": 2271,
        "lat": 46.781615599999995,
        "recvfirst": "2014-07-18 09:10:51"
    },
    {
        "status": "absent",
        "lasttime": "2014-07-18 09:48:01",
        "firsttime": "2014-07-18 09:48:01",
        "interval": 0,
        "duration": "00:00:00:00",
        "recvfirst": "2014-07-18 09:50:21"
    },
    {
        "status": "ok",
        "lasttime": "2014-07-20 12:36:07",
        "firsttime": "2014-07-18 10:02:39",
        "interval": 182008,
        "lon": 7.076272487465124,
        "duration": "02:02:33:28",
        "radius": 1318.1632404587922,
        "lat": 46.772127716753964,
        "recvfirst": "2014-07-18 10:09:59"
    }

];

data.sort(function (a, b) {
    return new Date(a.firsttime).getTime() - new Date(b.firsttime).getTime();
});



var tileLayers = {
    'openstreetmap': {
        url:'https://c.tile.openstreetmap.org/{z}/{x}/{y}.png',
        tileAttributes: {
            attribution: '© OpenStreetMap contributors'
        }
    },
    'satelite': {
        url:'http://otile{s}.mqcdn.com/tiles/1.0.0/sat/{z}/{x}/{y}.jpg',
        tileAttributes: {
            subdomains: '1234',
            attribution: 'Tiles Courtesy of MapQuest'

        }
    },
    'swissimage': {
        url:'http://tileserver.maptiler.com/swissimage25m/{z}/{x}/{y}.jpg',
        tileAttributes: {
            bounds: [[45.669276, 5.894335],
                [47.8415, 10.567622]],
            attribution: 'Federal Office of Topography, swisstopo',
            minAltitude: 6,
            maxAltitude: 12
        }
    },
    'cassini': {
        url:'http://tileserver.maptiler.com/cassini-terrestrial/{z}/{x}/{y}.jpg',
        tileAttributes: {
            attribution: ''

        }
    }
};


var earthCtrl = {
    current: data[0],
    next: function() {
        var currentIndex = data.indexOf(this.current);
        var newIndex = currentIndex + 1;
        if (newIndex >= data.length) {
            newIndex = 0;
        }
        this.current.marker.removeFrom(this.earth);
        this.current = data[newIndex];
        this.showCurrentMarker();
    },
    previous: function() {
        var currentIndex = data.indexOf(this.current);
        var newIndex = currentIndex - 1;
        if (newIndex < 0) {
            newIndex = data.length-1;
        }
        this.current.marker.removeFrom(this.earth);
        this.current = data[newIndex];
        this.showCurrentMarker();
    },
    showCurrentMarker: function() {
        var point = this.current;
        point.marker.addTo(this.earth);
        //this.earth.panTo([point.lat, point.lon], {duration: 1});
        document.getElementById('positionNumber').innerHTML = `${point.number}`;
        var date = new Date(point.firsttime).toLocaleDateString('de-CH');
        document.getElementById('positionDate').innerHTML = `${date}`;
    },
    initMarkers: function() {
        var count = 0;
        for (var i in data) {
            var point = data[i];
            var marker = WE.marker([point.lat, point.lon]);
            var timestamp = new Date(point.firsttime);
            var year = timestamp.getFullYear();
            var month = timestamp.getMonth()+1;
            var day = timestamp.getDate();
            var hour = timestamp.getHours();
            var minute = timestamp.getMinutes();
            var popupText = `
        <p><b>Date: ${year}-${month}-${day} ${hour}:${minute}</b></p>
        <p><b>Longitude: ${point.lon}</b><br>
            <b>Latitude: ${point.lat}</b></p>
        `;
            marker.bindPopup(popupText);
            point.marker = marker;
            point.number = ++count;
        }


    },
    createLayer: function(layerName) {
        return WE.tileLayer(tileLayers[layerName].url, tileLayers[layerName].tileAttributes);
    },
    initialize: function (layerName) {
        this.initMarkers();
        document.getElementById('earth_div').innerHTML = '';
        this.earth = new WE.map('earth_div', {center: [0,0], zoom: 0, atmosphere: false});
        this.layer = this.createLayer(layerName);
        this.layer.addTo(this.earth);
        this.earth.on('mousemove', function(e) {
            try {
                document.getElementById('coords').innerHTML = e.latlng.lat + ', ' + e.latlng.lng;
            } catch (err){}
        });
        this.showCurrentMarker();
    },
    changeLayer: function(newLayerName) {
        this.layer.removeFrom(this.earth);
        this.layer = this.createLayer(newLayerName);
        this.layer.addTo(this.earth);
    }

};



